//
//  SemaphoreTests.swift
//  Traffic Lights
//
//  Created by Razvan Balazs on 25/11/16.
//
//

import XCTest

class SemaphoreTests: XCTestCase {
    var sem1, sem2: Semaphore!
    
    override func setUp() {
        super.setUp()
        sem1 = Semaphore(1)
        sem2 = Semaphore(2)
        
    }
    
    override func tearDown() {
        sem1 = nil
        sem2 = nil
        
        super.tearDown()
    }
    
    func testExample() {
        // test color toggle
        sem1.toggleColor()
        XCTAssertTrue(sem1.color != .red)
        
        sem1.color = .yellow
        XCTAssertTrue(sem1.color == .yellow)
        
        sem1.toggleColor()
        XCTAssertTrue(sem1.color == .red)
        
        // test subscriptions
        var sem3: Semaphore? = Semaphore(3)
        sem1.oColor.subscribe(subscriber: sem3!,
                              next: { [weak sem3] newColor in
                                sem3?.color = newColor
                                XCTAssertEqual(sem3?.color, self.sem1.color)
        })
        sem3 = nil
        
        
        //test color change
        sem1.color = .green
        debugPrint(sem1.oColor.value)
        XCTAssertTrue(sem1.oColor.subscriptions.count == 0)
        
        
        let expectation = self.expectation(description: "test semaphore yellow not changed")

        sem2.color = .green
        sem2.toggleColor()
        let time = DispatchTime.now() + .seconds(3)
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            XCTAssertEqual(self.sem2.color, .yellow)
            expectation.fulfill()
        })
        
        
        let expectationOk = self.expectation(description: "test semaphore changed")

        let timeChanged = DispatchTime.now() + .seconds(6)
        DispatchQueue.main.asyncAfter(deadline: timeChanged, execute: {
            XCTAssertEqual(self.sem2.color, .red)
            expectationOk.fulfill()
        })
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
