//
//  IntersectionTests.swift
//  Traffic Lights
//
//  Created by  Razvan Balazs on 11/25/16.
//
//

import XCTest

class IntersectionTests: XCTestCase {
    var cross: Intersection!
    override func setUp() {
        super.setUp()
        cross = Intersection()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        cross.startTraffic()
        XCTAssertTrue(cross.running == true)
        
        cross.stop()
        XCTAssertTrue(cross.north.color == .red)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
