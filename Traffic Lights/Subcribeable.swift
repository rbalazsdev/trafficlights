//
//  Subcribeable.swift
//  Traffic Lights
//
//  Created by Razvan Balazs on 25/11/16.
//
//
// http://www.splinter.com.au/2015/07/23/swift-kvo-alternative/

import Foundation

class Property<T> {
    private var _value: T
    var value: T {
        get { return _value }
        set {
            _value = newValue
            tellSubscribers()
        }
    }
    
    init(_ value: T) {
        _value = value
    }
    
    var subscriptions = [Subscription<T>]()
    func subscribe(subscriber: AnyObject, next: @escaping (T) -> Void) {
        subscriptions.append(
            Subscription(subscriber: subscriber, next: next))
    }
    
    private func tellSubscribers() {
        subscriptions =
            subscriptions.flatMap(tellAndFilterSubscription)
    }
    
    private func tellAndFilterSubscription(subscription:
        Subscription<T>) -> Subscription<T>? {
        if subscription.subscriber != nil { // Subscriber exists.
            subscription.next(_value)
            return subscription
        } else { // Subscriber has gone; cull this subscription.
            return nil
        }
    }
}

struct Subscription<T> {
    weak var subscriber: AnyObject?
    let next: (T) -> Void
}
