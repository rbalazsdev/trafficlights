//
//  Semaphore.swift
//  Traffic Lights
//
//  Created by Razvan Balazs on 24/11/16.
//
//

import Foundation

protocol SemaphoreObservable {
    func didChangeColor(_ semaphore: Semaphore)
}

enum SemaphoreColor: String {
    case red, yellow, green
}


class Semaphore: NSObject {
    var id: Int
    var color: SemaphoreColor {
        didSet{
            observer?.didChangeColor(self)
            oColor.value = color
            debugPrint("id\(id) color:\(color)")
        }
    }
    var observer: SemaphoreObservable?
    let oColor = Property<SemaphoreColor>(.red)
    
    init(_ id:Int) {
        self.id = id
        color = .red
        super.init()
    }
    
    func toggleColor() {
        switch color {
        case .green:
            color = .yellow
            Timer.scheduledTimer(timeInterval: 5,
                                 target: self,
                                 selector: #selector(Semaphore.toggleColor),
                                 userInfo: nil,
                                 repeats: false)
        case .yellow:
            color = .red
        case .red:
            color = .green
        }
    }
}
