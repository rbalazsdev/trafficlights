//
//  SemaphoreView.swift
//  Traffic Lights
//
//  Created by Razvan Balazs on 24/11/16.
//
//

import Foundation
import UIKit

class SemaphoreView: UIView {
    @IBOutlet weak var imageView: UIImageView!
    
    func updateSemaphoreImage(_ semaphore: Semaphore) {
        self.imageView.image = semaphore.color.image()
        semaphore.oColor.subscribe(subscriber: self,
                                   next: { [weak self] semaphore  in
                                    self?.imageView.image = semaphore.image()
                                    self?.setNeedsLayout()
        })
    }
}

extension SemaphoreColor {
    func image() -> UIImage? {
        switch self {
        case .yellow:
            return UIImage(named: "amber")
        default:
            return UIImage(named: self.rawValue)
        }
    }
}
