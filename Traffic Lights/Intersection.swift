//
//  Intersection.swift
//  Traffic Lights
//
//  Created by  Razvan Balazs on 11/25/16.
//
//

import Foundation

class Intersection {
    var north = Semaphore(1)
    var east = Semaphore(3)
    var west = Semaphore(4)
    var south = Semaphore(2)
    
    var syncTimer: Timer?
    var running = false
    
    init() {
        let _ = [north, east].map({$0.observer = self})
        // play arround only with north and east, the rest will just sync with them.
        sync(west, after: east)
        sync(south, after: north)
    }
    
    func sync(_ first: Semaphore, after second: Semaphore) {
        second.oColor.subscribe(subscriber: first, next: { [weak first] newValue in
            first?.color = newValue
        })
    }
    
    func startTraffic(){
        running = true
        north.toggleColor()
    }
    
    func stop() {
        running = false
        syncTimer?.invalidate()
        north.color = .red
        east.color = .red
    }
}

extension Intersection: SemaphoreObservable {
    func didChangeColor(_ semaphore: Semaphore) {
        if !running {
            return
        }
        switch semaphore.color {
        case .green:
            // everytime a light is set to green, the light becomes the main one, the oposite one will change its colors depending on this one.
            syncTimer = Timer.scheduledTimer(timeInterval: 30,
                                             target: semaphore,
                                             selector: #selector(Semaphore.toggleColor),
                                             userInfo: nil,
                                             repeats: true)
            break
        case .red:
            syncTimer?.invalidate()
            if semaphore.id == 1 {
                east.toggleColor()
            }else {
                north.toggleColor()
            }
            break
        default:
            break
        }
    }
}
