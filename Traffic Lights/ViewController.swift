//
//  ViewController.swift
//  Traffic Lights
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var north: SemaphoreView!
    @IBOutlet weak var south: SemaphoreView!    
    @IBOutlet weak var east: SemaphoreView!
    @IBOutlet weak var west: SemaphoreView!
    
    let streets = Intersection()

    override func viewDidLoad() {
        super.viewDidLoad()
        updateSemphoresViews()
    }

    func updateSemphoresViews() {
        north.updateSemaphoreImage(streets.north)
        south.updateSemaphoreImage(streets.north)
        east.updateSemaphoreImage(streets.east)
        west.updateSemaphoreImage(streets.west)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func startStop(_ sender: Any) {
        if streets.running {
            streets.stop()
        } else {
            streets.startTraffic()
        }
    }
}


