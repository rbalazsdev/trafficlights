
### Traffic lights. ###

* Sample app to simulate an intersection. The intersection syncs the semaphores lights every 30 seconds, yellow light will appear for 5 seconds on switch from green to red. 

### How do I get set up? ###

* Download and run the project, all the sources are present. 

### Implementation details ###
* Intersection object will contain the semaphores, the logic is build on 2 semaphores (north and east), the rest will sync colors with them. Every time a semaphore starts (on green) the intersection will change the opposite's semaphore color in accordance. On switch to red, the other semaphore will start from green and it will be the followed one. 
The intersection is set as a delegate to the 2 semaphores and will get info about the change of color. 

* UIViews are subscribing to each of the semaphores and will change the containing image accordingly. 

* Unit tests are present for Intersection and Semaphore objects.
